/*
 *	Primary file for the API endpoints
 */


 // dependencies
 const http = require('http');
 const url = require('url');

 // create a server object to handle all requests and respond with a string
 const server = http.createServer(function (request, response) {
 	// ingore request if GET /favicon.ico
 	if(request.url === '/favicon.ico') {
 		response.end;
 		return;
 	}

 	// get the url and parse it
 	// syntax url.parse(<url>, <boolean>);
 	let parsedUrl = url.parse(request.url, true);
 	
 	// get the path
 	let pathname = parsedUrl.pathname;
 	let trimmedPath = pathname.replace(/^\/+|\/+$/g, '');
 	
 	// send the response
 	response.end('Hello NodeJS \n');

 	// log the request path
 	console.log('path' + trimmedPath);
 });

 // start the server, and have it listen on port 3000
 server.listen(3000, function () {
 	console.log('The server is running on port 3000 now');
 });